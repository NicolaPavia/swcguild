function hideResults() {
    document.getElementById("results").style.display = "none";
} 

function play() {
    var startingBet = document.getElementById("betInput").value;
    var currentBet = startingBet;
    var betsArray = [];

    while (currentBet > 0) {
    var die1 = Math.floor(Math.random() * 6) + 1;
    var die2 = Math.floor(Math.random() * 6) + 1;
    var diceRoll = die1 + die2;
        if(diceRoll != 7) {
            currentBet -= 1
        } else { 
            currentBet += 4
        }
        betsArray.push(currentBet)
    }

    var totalRoll = betsArray.length;
    var highestAmount = Math.max.apply(Math, betsArray);
    var highestPosition = betsArray.indexOf(highestAmount);
    var highestRoll = totalRoll - highestPosition;

    function showResults() {
    document.getElementById("resultTable").style.display = "inline";
    document.getElementById("playButton").innerHTML = "Play Again";
    document.getElementById("startingBet").innerHTML = "$" + startingBet;
    document.getElementById("rollCounter").innerHTML = totalRoll;
    document.getElementById("highestAmount").innerHTML = "$" +     highestAmount;
    document.getElementById("highestRoll").innerHTML = highestRoll;
    };

    showResults();
} 